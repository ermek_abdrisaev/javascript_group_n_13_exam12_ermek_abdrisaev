import { Picture, PictureData } from '../models/picture.model';
import { createAction, props } from '@ngrx/store';

export const fetchPicturesRequest = createAction(
  '[Pictures] Fetch Request');
export const fetchPicturesSuccess = createAction(
  '[Pictures] Fetch Success',
  props<{pictures: Picture[]}>()
);
export const fetchPicturesFailure = createAction(
  '[Pictures] Fetch Failure',
  props<{error: string}>()
);

export const fetchUsersPicturesRequest = createAction(
  '[Pictures] Fetch Users Request',
  props<{id: string}>()
);
export const fetchUsersPicturesSuccess = createAction(
  '[Pictures] Fetch Users Success',
  props<{pictures: Picture[]}>()
);
export const fetchUsersPicturesFailure = createAction(
  '[Pictures] Fetch Users Failure',
  props<{error: string}>()
);

export const fetchPictureRequest = createAction(
  '[Pictures] FetchOne Request',
  props<{id: string}>()
);
export const fetchPictureSuccess = createAction(
  '[Pictures] FetchOne Success',
  props<{picture: Picture}>()
);
export const fetchPictureFailure = createAction(
  '[Pictures] FetchOne Failure',
  props<{error: string}>()
);

export const createPicturesRequest = createAction(
  '[Pictures] Create Request',
  props<{pictureData: PictureData}>()
);
export const createPicturesSuccess = createAction(
  '[Pictures] Create Success'
);
export const createPicturesFailure = createAction(
  '[Pictures] Create Failure',
  props<{error: string}>()
);

export const deletePictureRequest = createAction(
  '[Picture] Delete Request',
  props<{ id: string }>());
export const deletePictureSuccess = createAction(
  '[Picture] Delete Success');
export const deletePictureFailure = createAction(
  '[Picture] Delete Failure',
  props<{ error: string }>());
