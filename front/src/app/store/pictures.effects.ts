import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { PicturesService } from '../services/pictures.service';
import {
  createPicturesFailure,
  createPicturesRequest, createPicturesSuccess, deletePictureFailure, deletePictureRequest, deletePictureSuccess,
  fetchPictureFailure,
  fetchPictureRequest,
  fetchPicturesFailure,
  fetchPicturesRequest,
  fetchPicturesSuccess, fetchPictureSuccess, fetchUsersPicturesFailure,
  fetchUsersPicturesRequest, fetchUsersPicturesSuccess
} from './pictures.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class PicturesEffect {
  constructor(
    private actions: Actions,
    private picturesService: PicturesService,
    private router: Router,
    private store: Store
  ) {
  }

  fetchPictures = createEffect(() => this.actions.pipe(
    ofType(fetchPicturesRequest),
    mergeMap(() => this.picturesService.getPictures().pipe(
      map(pictures => fetchPicturesSuccess({pictures})),
      catchError(() => of(fetchPicturesFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchPicture = createEffect(() => this.actions.pipe(
    ofType(fetchPictureRequest),
    mergeMap(({id}) => this.picturesService.getPicture(id).pipe(
      map(picture => fetchPictureSuccess({picture})),
      catchError(() => of(fetchPictureFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchUsersPictures = createEffect(() => this.actions.pipe(
    ofType(fetchUsersPicturesRequest),
    mergeMap(({id}) => this.picturesService.getUsersPictures(id).pipe(
      map(pictures => fetchUsersPicturesSuccess({pictures})),
      catchError(() => of(fetchUsersPicturesFailure({error: 'User doesnt have such pictures'})))
    ))
  ));

  createPicture = createEffect(() => this.actions.pipe(
    ofType(createPicturesRequest),
    mergeMap(({pictureData}) => this.picturesService.createPicture(pictureData).pipe(
        map(() => createPicturesSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(() => {
          return of(createPicturesFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  deletePicture = createEffect(() => this.actions.pipe(
    ofType(deletePictureRequest),
    mergeMap(({id}) => this.picturesService.removePicture(id).pipe(
      map(() => deletePictureSuccess()),
      tap(() => {
        this.store.dispatch(fetchPicturesRequest());
      }),
      catchError(() => of(deletePictureFailure({error: 'Not deleted'})))
    ))
  ));


}
