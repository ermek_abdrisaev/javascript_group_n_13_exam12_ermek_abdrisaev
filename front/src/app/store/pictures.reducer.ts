import { PictureState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createPicturesFailure,
  createPicturesRequest, createPicturesSuccess, deletePictureFailure, deletePictureRequest, deletePictureSuccess,
  fetchPictureFailure,
  fetchPictureRequest,
  fetchPicturesFailure,
  fetchPicturesRequest,
  fetchPicturesSuccess,
  fetchPictureSuccess,
  fetchUsersPicturesFailure,
  fetchUsersPicturesRequest,
  fetchUsersPicturesSuccess
} from './pictures.actions';

const initialState: PictureState = {
  pictures: [],
  picture: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const pictureReducer = createReducer(
  initialState,
  on(fetchPicturesRequest, state => ({...state, fetchLoading: true})),
  on(fetchPicturesSuccess, (state, {pictures}) => ({...state, fetchLoading: false, pictures})),
  on(fetchPicturesFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUsersPicturesRequest, state => ({...state, fetchLoading: true})),
  on(fetchUsersPicturesSuccess, (state, {pictures}) => ({...state, fetchLoading: false, pictures})),
  on(fetchUsersPicturesFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchPictureRequest, state => ({...state, fetchLoading: true})),
  on(fetchPictureSuccess, (state, {picture}) => ({...state, fetchLoading: false, picture})),
  on(fetchPictureFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createPicturesRequest, state => ({...state, createLoading: true})),
  on(createPicturesSuccess, state => ({...state, createLoading: false})),
  on(createPicturesFailure,  (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deletePictureRequest, state => ({...state, createLoading: true})),
  on(deletePictureSuccess, state => ({...state, createLoading: false})),
  on(deletePictureFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

)
