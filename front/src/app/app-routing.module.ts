import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { PicturesComponent } from './pages/pictures/pictures.component';
import { PictureCreateComponent } from './pages/picture-create/picture-create.component';
import { AuthorPicturesComponent } from './pages/author-pictures/author-pictures.component';

const routes: Routes = [
  {path: '', component: PicturesComponent},
  {
    path: 'picture-create',
    component: PictureCreateComponent,
    data: {roles: ['user']},
  },
  {path: 'author-pictures/:id', component: AuthorPicturesComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
