import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/user.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { PictureData } from '../../models/picture.model';
import { NgForm } from '@angular/forms';
import { createPicturesRequest } from '../../store/pictures.actions';

@Component({
  selector: 'app-picture-create',
  templateUrl: './picture-create.component.html',
  styleUrls: ['./picture-create.component.sass']
})
export class PictureCreateComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  user!: Observable<User | null>;
  newbe!: User | null;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.pictures.createLoading);
    this.error = store.select(state => state.pictures.createError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.user.subscribe(user => {
      this.newbe = user;
    })
  }

  onSubmit(){
    const newPicture = {
      user: <string>this.newbe?._id,
      title: this.form.value.title,
      image: this.form.value.image,
      author: this.form.value.author,
    }
    this.store.dispatch(createPicturesRequest({pictureData: newPicture}));
  }
}
