import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Picture } from '../../models/picture.model';
import { User } from '../../models/user.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { deletePictureRequest, fetchPictureRequest, fetchPicturesRequest } from '../../store/pictures.actions';
import { MatDialog} from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.sass']
})
export class PicturesComponent implements OnInit {
  pictures: Observable<Picture[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;

  constructor(private store: Store<AppState>, private dialog: MatDialog) {
    this.pictures = store.select(state => state.pictures.pictures);
    this.loading = store.select(state => state.pictures.fetchLoading);
    this.error = store.select(state => state.pictures.fetchError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPicturesRequest());
  }

  openDialog(id: string){
    this.store.dispatch(fetchPictureRequest({id}));
    this.dialog.open(DialogComponent);
  }

  onDelete(id: string){
    this.store.dispatch(deletePictureRequest({id}))
  }

}
