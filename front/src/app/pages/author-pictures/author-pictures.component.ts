import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/user.model';
import { Picture } from '../../models/picture.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { deletePictureRequest, fetchUsersPicturesRequest } from '../../store/pictures.actions';

@Component({
  selector: 'app-author-pictures',
  templateUrl: './author-pictures.component.html',
  styleUrls: ['./author-pictures.component.sass']
})
export class AuthorPicturesComponent implements OnInit {
  user!: Observable<User | null>;
  pictures: Observable<Picture[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  userId!: string;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
    this.pictures = store.select(state => state.pictures.pictures);
    this.loading = store.select(state => state.pictures.fetchLoading);
    this.error = store.select(state => state.pictures.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params['id'];
      this.store.dispatch((fetchUsersPicturesRequest({id: id})));
    });
    this.user.subscribe(user =>{
      this.userId = user?._id!;
    })
  }

  onDelete(id: string){
    this.store.dispatch(deletePictureRequest({id}));
  }
}
