import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { ApiPictureData, Picture } from '../../models/picture.model';
import { Observable } from 'rxjs';
import { fetchPictureRequest } from '../../store/pictures.actions';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.sass']
})
export class DialogComponent implements OnInit, OnDestroy {
  picture!: Observable<Picture | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  pictureId!: ApiPictureData;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any, private matDialogRef: MatDialogRef<DialogComponent>,
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.picture = store.select(state => state.pictures.picture);
    this.loading = store.select(state => state.pictures.fetchLoading);
    this.error = store.select(state => state.pictures.fetchError);
  }

  ngOnInit(): void {
    this.picture.subscribe(picture => {
      this.pictureId = <ApiPictureData>picture;
    })
  }

  ngOnDestroy(){
  this.matDialogRef.close();
  }


}
