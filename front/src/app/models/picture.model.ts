export class Picture {
  constructor(
    public _id: string,
    public title: string,
    public image: string,
    public user: {
      _id: string,
      displayName: string
    }
  ) {}
}

export interface PictureData {
  [key: string]: any,
  user: string,
  title: string,
  image: null | File,
}

export interface ApiPictureData {
  _id: string,
  title: string,
  image: string,
  user: {
    _id: string,
    displayName: string,
  }
}
