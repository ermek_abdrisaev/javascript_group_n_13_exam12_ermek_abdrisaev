import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';
import { AuthInterceptor } from './auth.interceptor';
import { HasRolesDirective } from './directives/has-roles.directive';
import { MatMenuModule } from '@angular/material/menu';
import { ImagePipe } from './pipes/image.pipe';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { PicturesComponent } from './pages/pictures/pictures.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { AppStoreModule } from './app-store.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { DialogComponent } from './pages/dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PictureCreateComponent } from './pages/picture-create/picture-create.component';
import { FormsModule } from '@angular/forms';
import { AuthorPicturesComponent } from './pages/author-pictures/author-pictures.component';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.fbAppId, {
        scope: 'email,public_profile'
      })
    }
  ]
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HasRolesDirective,
    CenteredCardComponent,
    ImagePipe,
    HasRolesDirective,
    FileInputComponent,
    LoginComponent,
    RegisterComponent,
    PicturesComponent,
    DialogComponent,
    PictureCreateComponent,
    AuthorPicturesComponent,
  ],
  entryComponents: [DialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    AppStoreModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatMenuModule,
    FlexLayoutModule,
    FlexModule,
    MatInputModule,
    SocialLoginModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatDialogModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
