const express = require('express');
const User = require('../models/User');
const mongoose = require("mongoose");
const auth = require("../middlewear/auth");
const axios = require("axios");
const {nanoid} = require("nanoid");
const config = require('../config');
const multer = require('multer');

const fs = require("fs");
const fetch = require('node-fetch');
const path = require("path");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const isPublished = false;
    const query = {};
    if (req.query.filter === 'image') {
      query.image = {$ne: null};
    }
    const users = await User.find();
    return res.send(users);
  } catch (e) {
    next(e);
  }
})

router.post('/', async (req, res, next) => {
  try {
    const userData = new User({
      email: req.body.email,
      password: req.body.password,
      displayName: req.body.displayName,
      avatar: null,
      token: req.body.token,
    });

    if (req.file) {
      userData.avatar = req.file.filename;
    }

    const user = new User(userData);

    user.generateToken();
    await user.save();

    return res.send(user);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/sessions', async (req, res, next) => {
  try {
    const user = await User.findOne({email: req.body.email});

    if (!user) {
      return res.status(400).send({error: 'Email not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(400).send({error: 'Password is wrong'});
    }

    user.generateToken();
    await user.save();

    return res.send(user);
  } catch (e) {
    next(e);
  }
});

router.delete('/sessions', async (req, res, next) => {
  try {
    const token = req.get('Authorization');
    const message = {message: 'OK'};

    if (!token) return res.send(message);

    const user = await User.findOne({token});

    if (!user) return res.send(message);

    user.generateToken();
    await user.save();

    return res.send(message);
  } catch (e) {
    next(e);
  }
})

router.post('/facebookLogin', async (req, res, next) => {
  const inputToken = req.body.authToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
  try {

    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({message: 'Facebook token incorrect'});
    }

    if (req.body.id !== response.data.data.user_id) {
      res.status(401).send({message: ' Wrong user id'})
    }

    let user = await User.findOne({facebookId: req.body.id});

    if (!user) {
      const pic = `${nanoid()}.jpg`;
      function downloadFile(url, path) {
        return fetch(url).then(res => {
          res.body.pipe(fs.createWriteStream(path));
        });
      }
      downloadFile(req.body.avatar, `./public/uploads/${pic}`)

      user = new User({
        email: req.body.email,
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
        avatar: pic
      });
    }

    user.generateToken();
    await user.save();

    return res.send(user);

  } catch (e) {
    next(e);
  }
})

module.exports = router;