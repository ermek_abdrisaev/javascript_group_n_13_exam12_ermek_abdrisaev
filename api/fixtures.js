const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require("nanoid");
const User = require('./models/User');
const Picture = require('./models/Picture');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, user2] = await User.create({
    email: 'user@user.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'Yusosha'
  }, {
    email: 'user2@user.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'Adosha',
  })

  await Picture.create({
    user: user,
    title: 'Lake',
    image: 'ushakovlake.jpg',
    author: 'Vlad Ushakov'
  }, {
    user: user,
    title: 'Mountains',
    image: 'ushakovkg.jpg',
    author: 'Vlad Ushakov'
  }, {
    user: user,
    title: 'Capm',
    image: 'ushakovcamp.jpg',
    author: 'Vlad Ushakov'
  }, {
    user: user2,
    title: 'African colors',
    image: 'africans.jpeg',
    author: 'Great African lover'
  }, {
    user: user2,
    title: 'Vietnam people',
    image: 'rehahn.jpeg',
    author: 'Great Afrivan lover'
  }, {
    user: user2,
    title: 'African nature',
    image: 'elephants.jpeg',
    author: 'Great Afrivan lover'
  })

  await mongoose.connection.close();
};

run().catch(e => console.error(e));